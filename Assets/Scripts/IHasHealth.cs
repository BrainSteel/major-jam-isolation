﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Represents an object with a health state, for
/// use by systems that monitor or visualize
/// interacting systems.
/// </summary>
public interface IHasHealth
{
    /// <summary>
    /// The current health state of the object.
    /// </summary>
    HealthState CurrentHealth { get; }
}
