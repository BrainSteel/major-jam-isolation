﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Linq;
using System.Collections.Generic;

/// <summary>
/// This static class provides common utilities for finding
/// objects needed by different game objects.
/// </summary>
public static class GlobalUtilities
{
    private static Scene _last_scene;
    private static GameObject[] _object_cache;

    private static void RefreshCache()
    {
        Scene active = SceneManager.GetActiveScene();
        if( active != _last_scene )
        {
            _last_scene = active;
            _object_cache = active.GetRootGameObjects();
        }
    }

    /// <summary>
    /// Gets the current player control object.
    /// </summary>
    /// <returns>
    /// A valid player control object.
    /// </returns>
    public static IPlayerControl GetPlayerControl()
    {
        RefreshCache();
        IPlayerControl ctrl;
        foreach (var obj in _object_cache)
        {
            if ( obj.TryGetComponent( out ctrl ) )
            {
                return ctrl;
            }
        }
        return null;
    }

    /// <summary>
    /// Gets the current player state object.
    /// </summary>
    /// <returns>
    /// A valid player state object.
    /// </returns>
    public static IPlayerState GetPlayerState()
    {
        RefreshCache();
        IPlayerState state;
        foreach( var obj in _object_cache )
        {
            if( obj.TryGetComponent( out state ) )
            {
                return state;
            }
        }
        return null;
    }

	/// <summary>
	/// Gets the main camera object.
	/// </summary>
	/// <returns>
	/// The first GameObject tagged MainCamera, if it exists, otherwise null.
	/// </returns>
	public static GameObject GetMainCameraObject() {
		GameObject[] cameras = GameObject.FindGameObjectsWithTag("MainCamera");

		if (cameras.Length < 1) {
			return null;
		} else {
			return cameras[0];
		}
	}

    /// <summary>
    /// Gets an array of all the GameObjects that have the given component.
    /// </summary>
    public static GameObject[] GetObjectsWithThisComponent<T>()
    {
        RefreshCache();
        List<GameObject> gameObjects = new List<GameObject>();
        T component;
        foreach (var obj in _object_cache)
        {
            if (obj.TryGetComponent<T>(out component))
            {
                gameObjects.Add(obj);
            }
        }

        return gameObjects.ToArray();
    }

    public static int GetUniqueID() { return UniqueIDCounter++; } // Works until 2 trillion. Should be good enough.
    private static int UniqueIDCounter = 0;
}
