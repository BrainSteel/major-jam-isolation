﻿using UnityEngine;
using System.Collections;

/// <summary>
/// This enumeration represents the health status of an object,
/// which impacts its in-game performance.
/// </summary>
public enum HealthState
{
    /// <summary>
    /// The object is healthy and functioning normally.
    /// </summary>
    Healthy,

    /// <summary>
    /// The object is damaged. The object may have reduced
    /// functionality, and will eventually fail completely.
    /// </summary>
    Damaged,

    /// <summary>
    /// The object is destroyed. The object does not function,
    /// and may not be repairable.
    /// </summary>
    Destroyed
}

/// <summary>
/// This enumeration represents buttons a player may press during
/// gameplay.
/// </summary>
public enum ActionButton
{
    /// <summary>
    /// The button the player presses to pause the game.
    /// </summary>
    Pause,

    /// <summary>
    /// The button the player presses to repair an interactable object.
    /// </summary>
    Repair
}
