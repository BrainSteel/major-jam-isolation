﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameState : MonoBehaviour
{
    IPlayerState _player_state;
    List<IRepairable> _interactables;

    // Use this for initialization
    void Start()
    {
        _player_state = GlobalUtilities.GetPlayerState();
        _interactables = new List<IRepairable>();
    }

    // Update is called once per frame
    void Update()
    {
        // Track important objects, allow objects to interact
    }
}
