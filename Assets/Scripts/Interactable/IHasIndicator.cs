﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public interface IHasIndicator
{
    /// <summary>
    /// Gets an ID to uniquely identify this object among all other indicatable objects.
    /// </summary>
    int IndicatorID { get; }

    /// <summary>
    /// Gets the icon to use for the indicator. Can return null if no indicator should be shown.
    /// </summary>
    Sprite GetIndicatorIcon();
}
