﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Represents an object (or component) that is repairable
/// by the player.
/// </summary>
public interface IRepairable : IHasHealth
{
    /// <summary>
    /// Indicates whether or not the object is being
    /// repaired.
    /// </summary>
    bool IsRepairing { get; }

    /// <summary>
    /// Indicates whether or not the object can be repaired
    /// from its current state.
    /// </summary>
    bool CanRepair { get; }

    // > May include other specific fields for repair duration or
    // health as a continuously varying quantity, need to settle
    // how interactions will work.
}
