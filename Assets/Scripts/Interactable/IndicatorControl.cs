﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class IndicatorControl : MonoBehaviour
{
    public Image IndicatorPrefab;
    /// <summary>
    /// Maps each indicatable object's ID to the GameObject that represents its indicator.
    /// </summary>
    private Dictionary<int, Image> _indicatorMap;

    // Start is called before the first frame update
    void Start()
    {
        _indicatorMap = new Dictionary<int, Image>();
    }

    void LateUpdate()
    {
        GameObject[] indicatableObjects = GlobalUtilities.GetObjectsWithThisComponent<IHasIndicator>();

        foreach(GameObject indicatableObject in indicatableObjects)
        {
            Sprite indicatorIcon = indicatableObject.GetComponent<IHasIndicator>()?.GetIndicatorIcon();
            int indicatorID = indicatableObject.GetComponent<IHasIndicator>()?.IndicatorID ?? -1;
            if (indicatorIcon == null)
            {
                RemoveIndicatorIfExists(indicatorID);
                continue;
            }

            Vector3 objectScreenPosition = Camera.main.WorldToScreenPoint(indicatableObject.transform.position);
            if (IsObjectOnScreen(objectScreenPosition))
            {
                // Currently just removing indicators when object is on screen. Could also switch to a different indicator.
                RemoveIndicatorIfExists(indicatorID);
            }
            else
            {
                if (objectScreenPosition.z < 0)  // Object is behind the camera. If this happens in 2D, it's probably not supposed to be displayed.
                {
                    RemoveIndicatorIfExists(indicatorID);
                    continue;
                }

                Vector3 screenCenter = new Vector3(Screen.width, Screen.height, 0) / 2;
                objectScreenPosition -= screenCenter; // NOTE: Make (0, 0) the center of screen instead of bottom left to make math easier

                float objectAngleFromCenter = Mathf.Atan2(objectScreenPosition.y, objectScreenPosition.x);
                objectAngleFromCenter -= 90 * Mathf.Deg2Rad; // Make 0 radians be straight up instead of to the right

                float cos = Mathf.Cos(objectAngleFromCenter);
                float sin = -Mathf.Sin(objectAngleFromCenter);
                objectScreenPosition = screenCenter + new Vector3(sin * 150, cos * 150, 0);

                float slope = cos / sin; // y=mx+b format

                objectScreenPosition = (cos > 0 ?
                    new Vector3(screenCenter.y / slope, screenCenter.y, 0) :
                    new Vector3(-screenCenter.y / slope, -screenCenter.y, 0));

                if (objectScreenPosition.x > screenCenter.x)
                {
                    objectScreenPosition = new Vector3(screenCenter.x, screenCenter.x * slope, 0);
                }
                else if (objectScreenPosition.x < -screenCenter.x)
                {
                    objectScreenPosition = new Vector3(-screenCenter.x, -screenCenter.x * slope, 0);
                }
                
                objectScreenPosition *= 0.8f; // Give indicators some padding from screen edge

                objectScreenPosition += screenCenter; // Move  (0, 0) back to the bottom left

                Image indicator = GetIndicator(indicatorID);
                indicator.sprite = indicatorIcon;
                indicator.transform.position = objectScreenPosition;
                indicator.transform.rotation = Quaternion.Euler(0, 0, objectAngleFromCenter * Mathf.Rad2Deg);
            }
        }
    }

    /// <summary>
    /// Removes the Indicator object from _indicatorMap if there is one.
    /// </summary>
    /// <param name="indicatorID"></param>
    private void RemoveIndicatorIfExists(int indicatorID)
    {
        if (_indicatorMap.ContainsKey(indicatorID))
        {
            Image indicator = _indicatorMap[indicatorID];
            _indicatorMap.Remove(indicatorID);
            Destroy(indicator.gameObject);
        }
    }

    /// <summary>
    /// Gets the indicator object for the given ID. Will create an empty indicator object if one does not exist.
    /// </summary>
    /// <param name="indicatorID"></param>
    /// <returns></returns>
    private Image GetIndicator(int indicatorID)
    {
        if (!_indicatorMap.ContainsKey(indicatorID))
        {
            Image indicator = Instantiate(IndicatorPrefab, transform);
            _indicatorMap.Add(indicatorID, indicator);
            return indicator;
        }
        else
        {
            return _indicatorMap[indicatorID];
        }
    }

    private bool IsObjectOnScreen(Vector3 objectScreenPosition)
    {
        return (objectScreenPosition.z > 0 &&
                objectScreenPosition.x > 0 && objectScreenPosition.x < Screen.width &&
                objectScreenPosition.y > 0 && objectScreenPosition.y < Screen.height);
    }
}
