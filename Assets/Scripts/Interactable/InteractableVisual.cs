﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// TODO > Probably want a script for each unique object, this is mostly a placeholder.
public class InteractableVisual : MonoBehaviour, IHasIndicator
{
    public Sprite IndicatorIcon;

    // Start is called before the first frame update
    void Start()
    {
        IndicatorID = GlobalUtilities.GetUniqueID();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public Sprite GetIndicatorIcon() { return IndicatorIcon; }
    public int IndicatorID { get; private set; }
}
