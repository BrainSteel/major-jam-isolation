﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMOD.Studio;
using FMODUnity;
using System;

public class FMOD_PlayerMovement : MonoBehaviour
{

    [SerializeField]
    [Range(0f, 1f)]
    public float normalizedSpeed;
	private float wobbleMin;
	private float wobbleMagnitude;
	private float wobbleSpeed;
	private bool wobbleUp;


    EventInstance movement;
    PARAMETER_ID  pid;
    PARAMETER_DESCRIPTION pd;

    private IPlayerState _player_state;


    void Start()
    {
        _player_state = GlobalUtilities.GetPlayerState();
		
		wobbleMin = 0.80f;
		wobbleUp = true;
		wobbleMagnitude = wobbleMin;

		movement = RuntimeManager.CreateInstance("event:/Player/Movement");
        movement.setParameterByName("Speed", 0f);
        RuntimeManager.AttachInstanceToGameObject(movement, transform, GetComponent<Rigidbody2D>());
        movement.start();
        movement.release();

        EventDescription ed;
        movement.getDescription(out ed);
        ed.getParameterDescriptionByName("Speed", out pd);
        pid = pd.id;


    }

    void LateUpdate()
    {
		float maxEngineSpeed = _player_state.MaxEngineSpeed;
		float playerSpeed = Math.Min(_player_state.Velocity.magnitude, maxEngineSpeed);
        normalizedSpeed = RemapValueInRange(playerSpeed, 0f, maxEngineSpeed, 0f, 1f);

		if (normalizedSpeed < wobbleMin) {
			movement.setParameterByID(pid, normalizedSpeed);
		} else {
			movement.setParameterByID(pid, wobbleMagnitude);

			float playerAcceleration = _player_state.Acceleration.magnitude;

			if (wobbleMagnitude >= 1f) {
				wobbleUp = false;
			} else if (wobbleMagnitude <= wobbleMin) {
				wobbleUp = true;
			}

			if (wobbleUp) {
				wobbleMagnitude += playerAcceleration * Time.deltaTime;
			} else {
				wobbleMagnitude -= playerAcceleration * Time.deltaTime;
			}
		}
        
    }

    private float RemapValueInRange(float currentValue, float prevMin, float prevMax, float newMin, float newMax)
    {
        return (currentValue - prevMin) / (prevMax - prevMin) * (newMax - newMin) + newMin;
    }
}
