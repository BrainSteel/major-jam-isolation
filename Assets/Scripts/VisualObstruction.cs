﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
public class VisualObstruction : MonoBehaviour
{
	SpriteRenderer _spriteRenderer;
	Transform _playerTransform;
	BoxCollider2D _myCollider;
	Color normalColor = new Color(1f, 1f, 1f, 1f);
	Color fadedColor = new Color(1f, 1f, 1f, 0.5f);

    // Start is called before the first frame update
    void Start()
    {
		_spriteRenderer = GetComponent(typeof(SpriteRenderer)) as SpriteRenderer;
		_playerTransform = GlobalUtilities.GetPlayerState().PlayerTransform;
    }

    // Update is called once per frame
    void Update()
    {
		RaycastHit2D result = Physics2D.Linecast(transform.position, _playerTransform.position);

		if (result.transform == _playerTransform && transform.position.y < _playerTransform.position.y) {
			_spriteRenderer.color = fadedColor;
		} else {
			_spriteRenderer.color = normalColor;
		}
    }
}
