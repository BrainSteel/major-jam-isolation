﻿using UnityEngine;
using System.Collections;

public class PlayerState : MonoBehaviour, IPlayerState, IPauseable
{
    private IPlayerControl _player_control;
    private Rigidbody2D _player_body;
    private float _total_rotation;

    public Transform PlayerTransform => gameObject.transform;

    public Vector2 Velocity => _player_body.velocity;

    public Vector2 Acceleration => _player_control.Acceleration * Facing;

    public HealthState CurrentHealth => HealthState.Healthy;

    public Vector2 Facing { get; private set; } = new Vector2( 1.0f, 0.0f );

    // This isn't 100% accurate, but it's pretty close. We could also just pick an arbitrary number that seems accurate enough, but this should generally work even if we change the drag/mass
    public float MaxEngineSpeed => ((_player_control.MaxAcceleration / _player_body.drag) - Time.fixedDeltaTime * _player_control.MaxAcceleration) / _player_body.mass;

    // Use this for initialization
    void Start()
    {
        _player_control = GlobalUtilities.GetPlayerControl();
        _player_body = GetComponent<Rigidbody2D>();
        _player_body.sharedMaterial = new PhysicsMaterial2D();
        _player_body.sharedMaterial.friction = 0.0f;
        _player_body.sharedMaterial.bounciness = 0.0f;
        _player_body.gravityScale = 0.0f;
        _player_body.angularDrag = 0.0f;
        _player_body.drag = 1.2f;
        _player_body.constraints = RigidbodyConstraints2D.FreezeRotation;
    }

    // Update is called once per frame
    void Update()
    {
        float steer_deg = _player_control.Steering * Mathf.PI / 180.0f * Time.deltaTime;

        // Subtracted here, since a positive rotation (player pressed right) should be clockwise.
        _total_rotation -= steer_deg;
        float cr = Mathf.Cos( _total_rotation );
        float sr = Mathf.Sin( _total_rotation );
        Facing = new Vector2( cr, sr );
        //transform.rotation = Quaternion.Euler( -45.0f, 0.0f, _total_rotation );

        _player_body.velocity += Acceleration * Time.deltaTime;
    }

    public void Pause()
    {
        throw new System.NotImplementedException();
    }

    public void Resume()
    {
        throw new System.NotImplementedException();
    }
}
