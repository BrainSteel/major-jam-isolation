﻿using UnityEngine;
using System.Collections;


[RequireComponent(typeof(Animator))]
public class PlayerVisual : MonoBehaviour, IPauseable
{
    IPlayerControl _player_control;
    IPlayerState _player_state;
	Animator _animator;

	// Use this for initialization
	void Start()
    {
        _player_control = GlobalUtilities.GetPlayerControl();
        _player_state = GlobalUtilities.GetPlayerState();
		_animator = GetComponent(typeof(Animator)) as Animator;
    }

    // Update is called once per frame
    void Update()
    {
		_animator.SetFloat("Speed", _player_state.Velocity.magnitude);
		int direction = (int) (360 + Vector2.SignedAngle(Vector2.right, _player_state.Velocity)) % 360;
		_animator.SetInteger("Direction", direction);
		_animator.SetInteger("Chance", Random.Range(0, 10));
    }

    public void Pause()
    {
        throw new System.NotImplementedException();
    }

    public void Resume()
    {
        throw new System.NotImplementedException();
    }
}
