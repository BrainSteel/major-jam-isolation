﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControl : MonoBehaviour, IPlayerControl
{
    public float ForwardAcceleration = 10;
    public float BackwardAcceleration = 5;
    public float RotationSpeed = 100;

    public float Steering { get; private set; }

    public float Acceleration { get; private set; }

    public float MaxAcceleration => ForwardAcceleration;

    public bool ButtonHeld( ActionButton button )
    {
        throw new System.NotImplementedException();
    }

    public bool ButtonPressed( ActionButton button )
    {
        throw new System.NotImplementedException();
    }

    public bool ButtonReleased( ActionButton button )
    {
        throw new System.NotImplementedException();
    }

    // Update is called once per frame
    void Update()
    {
        float verticalInput = Input.GetAxis("Vertical");
        Acceleration = verticalInput * (verticalInput > 0 ? ForwardAcceleration : BackwardAcceleration);
        Steering = Input.GetAxis("Horizontal") * RotationSpeed;
    }
}
