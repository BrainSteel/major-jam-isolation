﻿using UnityEngine;
using System.Collections;

/// <summary>
/// This interface describes the inputs a player may provide
/// during gameplay.
/// </summary>
public interface IPlayerControl
{
    /// <summary>
    /// This value indicates the rate at which the player is turning,
    /// in deg/s. May be positive or negative.
    /// </summary>
    float Steering { get; }

    /// <summary>
    /// This value indicates the rate at which the player is accelerating,
    /// in units/s^2. May be positive or negative.
    /// </summary>
    float Acceleration { get; }

    /// <summary>
    /// The maximum value that the player can accelerate in units/s^2.
    /// </summary>
    float MaxAcceleration { get; }

    /// <summary>
    /// Gets whether or not the specified button was pressed on this frame.
    /// </summary>
    /// <param name="button">
    /// The button to query.
    /// </param>
    /// <returns>
    /// True if the button was pressed on this frame, and false otherwise.
    /// </returns>
    bool ButtonPressed( ActionButton button );

    /// <summary>
    /// Gets whether or not the specified button was released on this frame.
    /// </summary>
    /// <param name="button">
    /// The button to query.
    /// </param>
    /// <returns>
    /// True if the button was released on this frame, and false otherwise.
    /// </returns>
    bool ButtonReleased( ActionButton button );

    /// <summary>
    /// Gets whether or not the specified button was held down during this frame.
    /// 
    /// On the specific frame that the button was pressed, this function returns
    /// true. On the frame the button was released, this function returns false.
    /// </summary>
    /// <param name="button">
    /// The button to query.
    /// </param>
    /// <returns>
    /// True if the button is currently held down and false otherwise.
    /// </returns>
    bool ButtonHeld( ActionButton button );
}
