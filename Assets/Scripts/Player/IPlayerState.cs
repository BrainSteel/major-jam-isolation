﻿using UnityEngine;
using System.Collections;

/// <summary>
/// This interface represents the public information about
/// the player's current state.
/// </summary>
public interface IPlayerState : IHasHealth
{
    /// <summary>
    /// The transform of the player.
    /// </summary>
    Transform PlayerTransform { get; }

    /// <summary>
    /// The velocity of the player, in 2D space.
    /// </summary>
    Vector2 Velocity { get; }

    /// <summary>
    /// The acceleration of the player, in 2D space.
    /// </summary>
    Vector2 Acceleration { get; }

    /// <summary>
    /// The direction the player is facing, in 2D space.
    /// This vector is normalized to have a length of 1.
    /// </summary>
    Vector2 Facing { get; }

    /// <summary>
    /// The maximum speed that the player can reach when the robot's engine is going full throttle.
    /// This is basically the maximum speed assuming default values for drag/acceleration.
    /// If the drag/acceleration variables are temporarily changed, the player's real max speed will also change
    /// but the max engine speed will stay the same.
    /// </summary>
    float MaxEngineSpeed { get; }
}
