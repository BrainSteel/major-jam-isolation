﻿using UnityEngine;
using System.Collections;

public class CameraControl : MonoBehaviour
{
    private Vector3 offset;
    IPlayerState _player_state;

    // Use this for initialization
    void Start()
    {
        _player_state = GlobalUtilities.GetPlayerState();
        offset = new Vector3( 0.0f, 0.0f, -10.0f );
    }

    // Update is called once per frame
    void Update()
    {
        // Follow the player.
        transform.position = _player_state.PlayerTransform.position + offset;
    }
}
