﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Represents an object that may be paused.
/// </summary>
public interface IPauseable
{
    /// <summary>
    /// Pause this object, freezing its state for later
    /// resumption.
    /// </summary>
    void Pause();

    /// <summary>
    /// Resume updates of this object.
    /// </summary>
    void Resume();
}
